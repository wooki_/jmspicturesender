package com.rulefinancial.ljanicki.picturesender;

import java.awt.Dimension;
import java.awt.TextArea;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.nio.file.Files;

import javax.jms.BytesMessage;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 * @author <a href="mailto:Lukasz.Janicki@rulefinancial.com">Lukasz Janicki</a>
 */
public class MainFrame extends JFrame {

	private TextArea chatArea;

	private Communication communication;

	private int returnVal;

	public MainFrame() {
		setResizable(false);
		setSize(new Dimension(100, 70));
		setLayout(new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		communication = new Communication();
		communication.establishConnection();
		prepareWindow();
	}

	/**
	 * 
	 */
	private void prepareWindow() {

		final JFileChooser fileChooser = new JFileChooser();
		fileChooser.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (returnVal == JFileChooser.APPROVE_OPTION) {
					communication.sendFile(fileChooser.getSelectedFile());
					JOptionPane.showMessageDialog(MainFrame.this,
							"Picture has been sent.");
				}
			}
		});

		JButton pickButton = new JButton("Pick picture");
		pickButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				returnVal = fileChooser.showDialog(MainFrame.this, "Send");
			}
		});
		add(pickButton);
	}

}
