package com.rulefinancial.ljanicki.picturesender;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;

import javax.jms.BytesMessage;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;

import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.commons.io.IOUtils;

/**
 * @author <a href="mailto:Lukasz.Janicki@rulefinancial.com">Lukasz Janicki</a>
 */
public class Communication {

	/**
	 * 
	 */
	public static final String SENDER_PROP = "sender";
	private MessageProducer producer;
	private MessageConsumer consumer;
	private Session session;

	public void establishConnection() {
		ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(
				ActiveMQConnection.DEFAULT_BROKER_URL);
		try {
			Connection connection = connectionFactory.createConnection();
			connection.start();
			session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
			Queue chatQueue = session.createQueue("pics");
			producer = session.createProducer(chatQueue);
		} catch (JMSException e) {
			e.printStackTrace();
		}
	}

	/**
	 * @param listener
	 * 
	 */
	public void setMessageHandler(MessageListener listener) {
		try {
			consumer.setMessageListener(listener);
		} catch (JMSException e) {
			e.printStackTrace();
		}
	}

	/**
	 * @param selectedFile
	 */
	public void sendFile(File selectedFile) {
		try {
			BytesMessage byteMessage = session.createBytesMessage();

			InputStream in = new FileInputStream(selectedFile);
			BufferedInputStream inBuf = new BufferedInputStream(in);
			int i;
			while ((i = inBuf.read()) != -1) {
				byteMessage.writeInt(i);
			}
			// adding an eof
			byteMessage.writeInt(-1);
			/*
			 * byteMessage.writeBytes(IOUtils.toByteArray(new FileReader(
			 * selectedFile)));
			 */
			producer.send(byteMessage);
		} catch (JMSException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
